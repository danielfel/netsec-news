#!/usr/bin/env python
import praw
import secrets
import colorama
import argparse


my_user_agent = 'news scrapper'
reddit = praw.Reddit(user_agent=my_user_agent,
                     client_id=secrets.my_client_id,
                     client_secret=secrets.my_client_secret
                     )


def arguments():
    parser = argparse.ArgumentParser(
        description="Adds a set of arguments from user"
    )
    parser.add_argument("-l",
                        "--limit",
                        dest='limit_posts',
                        type=int,
                        default=10,
                        help="Define number to limit posts")
    parser.add_argument("-s",
                        "--sub-reddit",
                        dest='sub_reddit',
                        type=str,
                        default="netsec+pwend",
                        help='The subreddit you want to pull the posts from')
    args = parser.parse_args()
    return args


def scrapper(args):
    submissions = reddit.subreddit(args.sub_reddit).new(limit=args.limit_posts)
    for submission in submissions:
        print(colorama.Fore.GREEN + 80*"-")
        print(colorama.Fore.CYAN + "Title: "), submission.title[0:80]
        print(colorama.Fore.MAGENTA + "Link to Reddit: "), ("https://www.reddit.com"+submission.permalink)
        print("External Url: "), submission.url
        print
        print(colorama.Fore.BLUE), submission.selftext[0:450]
        print(colorama.Style.RESET_ALL + "")


def main():
    try:
        scrapper(arguments())
    except Exception:
        print("Wrong subreddit")
if __name__ == '__main__':
    main()

#!/usr/bin/env python

from lxml import html
import requests
import colorama


page = requests.get('http://thehackernews.com/')
tree = html.fromstring(page.content)

def main():
    title = tree.xpath('//*[@id="Blog1"]/div[1]/article/span/h2/a/text()')
    link = tree.xpath('//*[@id="Blog1"]/div[1]/article/span/h2/a/@href')
    desc = tree.xpath('//*[@id="Blog1"]/div[1]/article/span/div/text()')
    for t, l, d  in zip(title, link, desc):
        print (colorama.Fore.GREEN + 80*"-")
        print (colorama.Fore.CYAN + "Title: "), t[0:80]
        print (colorama.Fore.MAGENTA + "Link to article: "), l
        print
        print (colorama.Fore.BLUE), d[0:450]
        print (colorama.Style.RESET_ALL + "")




if __name__ == '__main__':
    main()
